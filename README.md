# Fedoraproject.org Content Repository

Log in to the fedoraproject.org website [Content Managment System](https://fedora.gitlab.io/websites-apps/fedora-websites/cms/fedoraproject.org/admin/) with your GitLab account.

Website code repository: [fedora-websites-3.0](https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0)

## License

This project is licensed under the [GNU Affero General Public License v3.0 only](LICENSE) (AGPL-3.0-only).
